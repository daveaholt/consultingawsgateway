using System;
using System.IO;
using System.Threading.Tasks;
using Amazon.Lambda.TestUtilities;
using Amazon.Lambda.APIGatewayEvents;
using ConsultingAWS;
using ConsultingAWS.Client;
using Newtonsoft.Json;
using NUnit.Framework;


namespace ConsultingAWSGateway.Tests
{
    [TestFixture]
    public class ExpressWebControllerTests
    {
        [Test]
        public async Task TestPost()
        {
            var lambdaFunction = new LambdaEntryPoint();
            
            var requestStr = File.ReadAllText("./SampleRequests/ExpressWebController-Post.json");
            var request = JsonConvert.DeserializeObject<APIGatewayProxyRequest>(requestStr);
            var context = new TestLambdaContext();
            var response = await lambdaFunction.FunctionHandlerAsync(request, context);

            Assert.That(response.StatusCode, Is.EqualTo(200));
            Assert.True(response.Headers.ContainsKey("Content-Type"));
            Assert.That(response.Headers["Content-Type"], Is.EqualTo("application/json; charset=utf-8"));

            var responseObject = JsonConvert.DeserializeObject<CustomizationsResponse>(response.Body);
            Assert.NotNull(responseObject);
            Assert.That(responseObject.OrgCode, Is.EqualTo("SANDBOX"));
        }
    }
}
