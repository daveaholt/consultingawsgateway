﻿using System;
using System.IO;
using System.Threading.Tasks;
using Amazon;
using Amazon.Lambda;
using Amazon.Lambda.Model;
using ConsultingAWS.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Environment = System.Environment;

namespace ConsultingAWS.Controllers
{
    [Route("[controller]/")]
    public class ExpressWebController : Controller
    {
        private readonly ConsultingLogger _logger;
        private readonly RegionEndpoint _region;

        public ExpressWebController()
        {
            _logger = new ConsultingLogger(Environment.GetEnvironmentVariable("LogLevel")?.ToLower());
            var regionString = Environment.GetEnvironmentVariable("Region");
            if (string.IsNullOrEmpty(regionString))
            {
                regionString = "us-east-2";
            }

            switch (regionString.ToLower())
            {
                case "us-east-1":
                    _region = RegionEndpoint.USEast1;
                    break;
                case "us-east-2":
                    _region = RegionEndpoint.USEast2;
                    break;
                default:
                    throw new Exception("Invalid or unsupported AWS region.");
            }
        }

        /// <summary>
        /// Route for all ExpressWeb one off customizations.
        /// </summary>
        /// <param name="customizationsRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Customizations")]
        public CustomizationsResponse Customizations(CustomizationsRequest customizationsRequest)
        {
            _logger.Debug($"ExpressWeb/Customizations endpoint begun: {DateTime.Now.ToLongDateString()}");
            CustomizationsResponse response;

            var requestBody = GetObjectFromStream<CustomizationsRequest>(Request);

            _logger.Debug($"For OrgCode: {requestBody.OrgCode}");
            _logger.Debug($"Targeting Function: {requestBody.FunctionName}");

            if (string.IsNullOrEmpty(requestBody.FunctionName))
            {
                _logger.Error("Function Name not defined.  Returning without processing.");
                response = new CustomizationsResponse
                {
                    OrgCode = requestBody.OrgCode,
                    ResultType = ResultType.Error,
                    ErrorMessage = new ErrorMessage {Message = "No FunctionName found.", Detail = $"ExpressWeb/Customizations was called for org:{requestBody.OrgCode} with no FunctionName defined."}
                };
            }
            else
            {
                try
                {
                    var lambdaConfig = new AmazonLambdaConfig
                    {
                        RegionEndpoint = _region
                    };

                    var payloadString = JsonConvert.SerializeObject(requestBody);
                    var lambdaRequest = new InvokeRequest
                    {
                        FunctionName = requestBody.FunctionName,
                        Payload = payloadString
                    };

                    InvokeResponse lambdaResponse = null;
                    var lambdaTask = Task.Run(async () =>
                    {
                        using (var client = new AmazonLambdaClient("AKIAIXJPCZUKQGGHU7HQ", "molGccj3CA36JQVp9faRamBpEZee9RSBvbJfDL0E", lambdaConfig))
                        {
                            lambdaResponse = await client.InvokeAsync(lambdaRequest);
                        }
                    });
                    lambdaTask.Wait();

                    string lambdaResultAsString;
                    using (var sr = new StreamReader(lambdaResponse.Payload))
                    {
                        lambdaResultAsString = sr.ReadToEnd();
                    }

                    if (!string.IsNullOrEmpty(lambdaResultAsString))
                    {
                        response = JsonConvert.DeserializeObject<CustomizationsResponse>(lambdaResultAsString);
                    }
                    else
                    {
                        var message = $"Lambda function {requestBody.FunctionName} returned no data";
                        var detail = $"No data returned from call to Lambda function {requestBody.FunctionName} with payload {payloadString}";

                        _logger.Error(message);
                        _logger.Trace(detail);

                        response = new CustomizationsResponse
                        {
                            OrgCode = requestBody.OrgCode,
                            ResultType = ResultType.Error,
                            ErrorMessage = new ErrorMessage {Message = message, Detail = detail}
                        };
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e.Message);
                    _logger.Trace(e.ToString());
                    response = new CustomizationsResponse
                    {
                        OrgCode = requestBody.OrgCode,
                        ResultType = ResultType.Error,
                        ErrorMessage = new ErrorMessage {Message = e.Message, Detail = e.ToString()}
                    };
                }
            }

            _logger.Debug($"ExpressWeb/Customizations endpoint ended: {DateTime.Now.ToLongDateString()}");
            return response;
        }

        /// <summary>
        /// Route for the Express Web Contribution RoundUp plugin
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("RoundUp")]
        public RoundUpResponse RoundUp(RoundUpRequest request)
        {
            return new RoundUpResponse();
        }

        private static T GetObjectFromStream<T>(HttpRequest request)
        {
            var response = default(T);
            try
            {
                var t = Task.Run(async () =>
                {
                    var seekableStream = new MemoryStream();
                    await request.Body.CopyToAsync(seekableStream);
                    seekableStream.Position = 0;
                    response = JsonSerializer.Create().Deserialize<T>(new JsonTextReader(new StreamReader(seekableStream)));
                });
                t.Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return response;
        }
    }
}