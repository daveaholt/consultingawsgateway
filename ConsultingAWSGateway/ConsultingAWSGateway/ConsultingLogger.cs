﻿using Amazon.Lambda.Core;

namespace ConsultingAWS
{
    public interface IConsultingLogger
    {
        void Debug(string message);
        void Error(string message);
        void Fatal(string message);
        void Info(string message);
        void Trace(string message);
        void Warn(string message);
    }

    public class ConsultingLogger : IConsultingLogger
    {
        private readonly int _logLevel;

        public ConsultingLogger(string logLevel)
        {
            switch (logLevel)
            {
                case "trace":
                    _logLevel = 100;
                    break;
                case "debug":
                    _logLevel = 200;
                    break;
                case "info":
                    _logLevel = 100;
                    break;
                case "warn":
                    _logLevel = 100;
                    break;
                case "error":
                    _logLevel = 500;
                    break;
                case "fatal":
                    _logLevel = 600;
                    break;
                default:
                    Log($"Unknown log level '{logLevel}'. Defaulting to error");
                    _logLevel = 500;
                    break;
            }
        }

        public void Trace(string message)
        {
            if (this._logLevel > 100)
                return;
            Log(message);
        }

        public void Debug(string message)
        {
            if (this._logLevel > 200)
                return;
            Log(message);
        }

        public void Info(string message)
        {
            if (this._logLevel > 300)
                return;
            Log(message);
        }

        public void Warn(string message)
        {
            if (this._logLevel > 400)
                return;
            Log(message);
        }

        public void Error(string message)
        {
            if (this._logLevel > 500)
                return;
            Log(message);
        }

        public void Fatal(string message)
        {
            if (this._logLevel > 600)
                return;
            Log(message);
        }

        private static void Log(string message)
        {
            LambdaLogger.Log(message);
        }
    }
}