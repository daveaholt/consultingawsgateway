﻿using System.Collections.Generic;

namespace ConsultingAWS.Client
{
    public class CustomizationsResponse
    {
        public int ResultType { get; set; }
        public string OrgCode { get; set; }
        public ErrorMessage ErrorMessage { get; set; }
        public string FunctionName { get; set; }
        public string HtmlContent { get; set; }
        public IEnumerable<CustomDataObject> Data { get; set; }
    }
}