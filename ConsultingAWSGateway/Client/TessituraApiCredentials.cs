﻿namespace ConsultingAWS.Client
{
    public class TessituraApiCredentials
    {
        public string OrgCode { get; set; }
        public string UserName { get; set; }
        public string UserGroup { get; set; }
        public string Password { get; set; }
        public string ProxyUser { get; set; }
        public string MachineName { get; set; }
        public string LiveBaseUri { get; set; }
        public string TestBaseUri { get; set; }
        public string DevBaseUri { get; set; }
    }
}