﻿namespace ConsultingAWS.Client
{
    public class RoundUpRequest
    {
        public string OrgCode { get; set; }
        public string FunctionName { get; set; }
        public string Auth { get; set; }
        public string UriBase { get; set; }
    }
}