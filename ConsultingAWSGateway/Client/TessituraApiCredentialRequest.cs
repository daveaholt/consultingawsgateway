﻿using Amazon;

namespace ConsultingAWS.Client
{
    public class TessituraApiCredentialRequest
    {
        public string OrgCode { get; set; }
        public RegionEndpoint Region { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
    }
}