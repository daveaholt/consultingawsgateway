﻿namespace ConsultingAWS.Client
{
    public class RoundUpResponse
    {
        public ResultType ResultType { get; set; }
        public string OrgCode { get; set; }
        public string FunctionName { get; set; }
        public string HtmlContent { get; set; }
    }
}