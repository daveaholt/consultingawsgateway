﻿namespace ConsultingAWS.Client
{
    public class CustomDataObject
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}