﻿namespace ConsultingAWS.Client
{
    public class ErrorMessage
    {
        public string Message { get; set; }
        public string Detail { get; set; }
    }
}