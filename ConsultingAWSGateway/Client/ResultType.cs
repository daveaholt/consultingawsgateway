﻿using System.Reflection.Metadata;

namespace ConsultingAWS.Client
{
    public class ResultType
    {
        public const int Error = 0;
        public const int HtmlDisplay = 1;
        public const int Redirect = 2;
        public const int FunctionCall = 3;
    }
}