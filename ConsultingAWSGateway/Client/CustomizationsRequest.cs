﻿using System.Collections;
using System.Collections.Generic;

namespace ConsultingAWS.Client
{
    public class CustomizationsRequest
    {
        public string OrgCode { get; set; }
        public string FunctionName { get; set; }
        public TessituraApiCredentials Auth { get; set; }
        public IEnumerable<CustomDataObject> Data { get; set; }
    }
}