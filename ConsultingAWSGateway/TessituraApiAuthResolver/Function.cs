using System;
using System.IO;
using Amazon.Lambda.Core;
using ConsultingAWS.Client;
using Amazon.SecretsManager;
using Amazon.SecretsManager.Model;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace TessituraApiAuthResolver
{
    public class Function
    {

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public TessituraApiCredentials FunctionHandler(TessituraApiCredentialRequest request, ILambdaContext context)
        {
            if (string.IsNullOrEmpty(request.OrgCode))
            {
                throw new Exception("TessituraApiAuthResolver called with null or empty orgCode.");
            }

            TessituraApiCredentials response;

            if (request.OrgCode == "SANDBOX")
            {
                response = new TessituraApiCredentials
                {
                    UserName = "admin",
                    UserGroup = "allright",
                    MachineName = "machine",
                    Password = "",
                    LiveBaseUri = "https://sandbox-web1.tessituranetworkdev.com/TessituraService",
                    OrgCode = request.OrgCode,
                    ProxyUser = null
                };
            }
            else
            {
                if (string.IsNullOrEmpty(request.AccessKey))
                {
                    throw new Exception("TessituraApiAuthResolver called with null or empty AccessKey.");
                }
                if (string.IsNullOrEmpty(request.SecretKey))
                {
                    throw new Exception("TessituraApiAuthResolver called with null or empty SecretKey.");
                }
                if (request.Region == null)
                {
                    throw new Exception("TessituraApiAuthResolver called with null or empty Region.");
                }
                IAmazonSecretsManager client = new AmazonSecretsManagerClient(request.AccessKey, request.SecretKey, request.Region);
                var secretRequest = new GetSecretValueRequest
                {
                    SecretId = request.OrgCode,
                    VersionStage = "AWSCURRENT"
                };
                GetSecretValueResponse secretResponse;
                try
                {
                    secretResponse = client.GetSecretValueAsync(secretRequest).Result;
                }
                catch (DecryptionFailureException e)
                {
                    // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
                    // Deal with the exception here, and/or rethrow at your discretion.
                    Console.Write(e);
                    throw;
                }
                catch (InternalServiceErrorException e)
                {
                    // An error occurred on the server side.
                    // Deal with the exception here, and/or rethrow at your discretion.
                    Console.Write(e);
                    throw;
                }
                catch (InvalidParameterException e)
                {
                    // You provided an invalid value for a parameter.
                    // Deal with the exception here, and/or rethrow at your discretion
                    Console.Write(e);
                    throw;
                }
                catch (InvalidRequestException e)
                {
                    // You provided a parameter value that is not valid for the current state of the resource.
                    // Deal with the exception here, and/or rethrow at your discretion.
                    Console.Write(e);
                    throw;
                }
                catch (ResourceNotFoundException e)
                {
                    // We can't find the resource that you asked for.
                    // Deal with the exception here, and/or rethrow at your discretion.
                    Console.Write(e);
                    throw;
                }
                catch (AggregateException ae)
                {
                    // More than one of the above exceptions were triggered.
                    // Deal with the exception here, and/or rethrow at your discretion.
                    Console.Write(ae);
                    throw;
                }
                string jsonSecret;
                if (secretResponse.SecretString != null)
                {
                    jsonSecret = secretResponse.SecretString;
                }
                else
                {
                    var memoryStream = secretResponse.SecretBinary;
                    var reader = new StreamReader(memoryStream);
                    jsonSecret = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(reader.ReadToEnd()));
                }
                response = JsonConvert.DeserializeObject<TessituraApiCredentials>(jsonSecret);
            }
            return response;
        }
    }
}
