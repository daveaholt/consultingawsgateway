using Amazon.Lambda.Core;
using ConsultingAWS.Client;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace ConsultingAWSTestFunction
{
    public class Function
    {
        
        /// <summary>
        /// A simple function that confirms throughput on customizations routed via the ConsultingAWSGateway
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public CustomizationsResponse FunctionHandler(CustomizationsRequest input, ILambdaContext context)
        {
            var response = new CustomizationsResponse
            {
                OrgCode = input.OrgCode,
                FunctionName = input.FunctionName,
                ErrorMessage = null,
                ResultType = ResultType.HtmlDisplay,
                HtmlContent = "<p><strong>Woot!</strong> we made it back from the test function.</p>"
            };
            return response;
        }
    }
}
